use std::{
    fs::{self, File},
    io::{Error, Read},
};
use wasmer::{imports, Instance, Module, Native, Store, LLVM};

fn main() -> Result<(), Error> {
    println!("Hello, world!");

    // Create Runtime
    let compiler = LLVM::default();
    let store = Store::new(&Native::new(compiler).engine());

    // Load module
    let bytes = load_module("bin/core.wasm");

    let module = Module::new(&store, bytes).unwrap();

    // Create imports
    let imports = imports! {};

    // Create instance
    let instance = Instance::new(&module, &imports).unwrap();

    // reference function
    let test_wasm = instance.exports.get_function("test_wasm").unwrap();

    // exec function
    test_wasm.call(&vec![]).unwrap();

    // Create Headless Game

    // Create View

    Ok(())
}

fn load_module(path: &str) -> Vec<u8> {
    // Open file
    let mut f = File::open(&path).expect("no file found");
    // Get metadata
    let metadata = fs::metadata(&path).expect("unable to read metadata");
    // Create buffer
    let mut buffer = vec![0; metadata.len() as usize];
    // Read file into buffer
    f.read_exact(&mut buffer).expect("buffer overflow");
    // Return buffer
    buffer
}
